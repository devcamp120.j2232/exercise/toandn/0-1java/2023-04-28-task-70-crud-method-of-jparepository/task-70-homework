"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
// mảng chapter
var gListChapterObjects = [];
var gChapterObject = {};
var gSTT = 0;
var gId;

const gBASE_URL = "http://localhost:8080/chapter";

const ARRAY_NAME_COL = [
  "Stt",
  "chapterCode",
  "chapterName",
  "chapterIntro",
  "nameTranslator",
  "chapterPage",
  "action",
];
//khai báo các cột
const gCOL_NO = 0;
const gCOL_CHAPTER_CODE = 1;
const gCOL_CHAPTER_NAME = 2;
const gCOL_CHAPTER_INTRO = 3;
const gCOL_NAME_TRANSLATOR = 4;
const gCOL_CHAPTER_PAGE = 5;
const gCOL_ACTION = 6;

// khởi tạo datatable  - chưa có data
var gDataTable = $("#chapter-table").DataTable({
  columns: [
    { data: ARRAY_NAME_COL[gCOL_NO] },
    { data: ARRAY_NAME_COL[gCOL_CHAPTER_CODE] },
    { data: ARRAY_NAME_COL[gCOL_CHAPTER_NAME] },
    { data: ARRAY_NAME_COL[gCOL_CHAPTER_INTRO] },
    { data: ARRAY_NAME_COL[gCOL_NAME_TRANSLATOR] },
    { data: ARRAY_NAME_COL[gCOL_CHAPTER_PAGE] },
    { data: ARRAY_NAME_COL[gCOL_ACTION] },
  ],
  columnDefs: [
    //định nghĩa các cột cần hiện ra
    {
      targets: gCOL_NO, //cột STT
      className: "text-center text-primary",
      /*render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1; */
      render: function () {
        gSTT++;
        return gSTT;
      },
    },
    {
      targets: gCOL_CHAPTER_CODE,
      className: "text-center",
    },
    {
      targets: gCOL_CHAPTER_PAGE,
      className: "text-center",
    },
    {
      targets: gCOL_ACTION, //cột Action
      className: "text-center",
      defaultContent: `
                            <button id= "update-chapter"  <i title = 'DEATIL' class="fa-solid fa-file-circle-check text-info" style = "cursor:pointer; border: 1px white;"></i></button> &nbsp;
                            <button id= "delete-chapter" <i title = 'Delete' class="fa-solid fa-trash text-danger" style = "cursor:pointer;border: 1px white;"></i></button>`,
    },
  ],
});

/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
onPageLoading();

//gán sự kiện tạo chapter mới
$("#add-infor-chapter").on("click", function () {
  onBtnAddInforClick();
});
// gán sự kiện cho nút Confirm create (trên modal)
$("#btn-create-confirm").on("click", function () {
  onBtnConfirmCreateClick();
});
// gán sự kiện cho nút Cancel (trên modal)
$("#btn-create-close").on("click", function () {
  onBtnCloseCreateClick();
});

// 3 - U: gán sự kiện Update - khi nhấn nút edit
$("#chapter-table").on("click", "#update-chapter", function () {
  onBtnEditClick(this);
});
// gán sự kiện cho nút Confirm edit (trên modal)
$("#btn-update-confirm").on("click", function () {
  onBtnConfirmEditClick();
});

$("#chapter-table").on("click", "#delete-chapter", function () {
  onBtnDeleteClick(this);
});
// gán sự kiện cho nút Confirm edit (trên modal)
$("#btn-delete-confirm").on("click", function () {
  onBtnConfirmDeleteClick();
});
// gán sự kiện cho nút Cancel (trên modal)
$("#btn-delete-cancel").on("click", function () {
  onBtnCancelDeleteClick();
});

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
  "use strict";
  // lấy data từ server
  callApiToGetListData();
}

//hàm call API to get all chapter
function callApiToGetListData() {
  $.ajax({
    url: gBASE_URL + "/all",
    type: "GET",
    dataType: "json",
    success: function (responseObject) {
      gListChapterObjects = responseObject;
      console.log(responseObject);
      DisplayDataToTable(responseObject);
    },
    error: function (error) {
      console.assert(error.responseText);
    },
  });
}

//hàm xử lý sự kiện add infor chapter
function onBtnAddInforClick() {
  "use strict";
  console.log("Button add được click");
  $("#create-chapter-modal").modal("show");
}

// Hàm xử lý sự kiện khi icon close trên bảng đc click
function onBtnCloseCreateClick() {
  "use strict";
  $("#create-chapter-modal").modal("hide");
}

//Hàm xử lý khi nhấn nút confirm trên modal
function onBtnConfirmCreateClick() {
  // khai báo đối tượng
  var vObjData = {
    chapterCode: "",
    chapterName: "",
    chapterIntro: "",
    nameTranslator: "",
    chapterPage: 0,
  };
  // B1: Thu thập dữ liệu
  getCreateData(vObjData);
  // B2: Validate update
  var vIsValidate = validateData(vObjData);
  if (vIsValidate) {
    // B3: update order
    $.ajax({
      url: gBASE_URL + "/create",
      type: "POST",
      contentType: "application/json;charset=UTF-8",
      data: JSON.stringify(vObjData),
      success: function (paramRes) {
        // B4: xử lý front-end
        handleCreateSuccess();
      },
      error: function (paramErr) {
        console.log(paramErr.responseText);
      },
    });
  }
}

// Hàm xử lý sự kiện khi icon edit trên bảng đc click
function onBtnEditClick(paramBtnEdit) {
  "use strict";
  console.log("Button edit được click");
  // hàm dựa vào button edit (edit or delete) xác định đc id
  var vTableRow = $(paramBtnEdit).parents("tr");
  var vRowData = gDataTable.row(vTableRow).data();
  gId = vRowData.id;

  console.log("ID lấy được: " + gId);
  getChapterById(gId);
}

//Hàm xử lý khi nhấn nút confirm edit trên modal
function onBtnConfirmEditClick() {
  // khai báo đối tượng
  var vObjData = {
    chapterCode: "",
    chapterName: "",
    chapterIntro: "",
    nameTranslator: "",
    chapterPage: 0,
  };
  // B1: Thu thập dữ liệu
  getUpdateData(vObjData);
  // B2: Validate update
  var vIsValidate = validateData(vObjData);
  if (vIsValidate) {
    // B3: update
    $.ajax({
      url: gBASE_URL + "/update/" + gId,
      type: "PUT",
      contentType: "application/json;charset=UTF-8",
      data: JSON.stringify(vObjData),
      success: function (paramRes) {
        // B4: xử lý front-end
        handleUpdateSuccess();
      },
      error: function (paramErr) {
        console.log(paramErr.responseText);
      },
    });
  }
}

// Hàm xử lý sự kiện khi icon delete trên bảng đc click
function onBtnDeleteClick(paramBtnDelete) {
  "use strict";
  console.log("Button delete được click");
  // hàm dựa vào button detail (edit or delete) xác định đc id
  var vTableRow = $(paramBtnDelete).parents("tr");
  var vRowData = gDataTable.row(vTableRow).data();
  gId = vRowData.id;
  console.log("ID lấy được: " + gId);
  // hiển thị modal lên
  $("#delete-chapter-modal").modal("show");
}

//sự kiện confirm delete order
function onBtnConfirmDeleteClick() {
  deleteChapterById(gId);
}
//cancel delete order
function onBtnCancelDeleteClick() {
  $("#delete-chapter-modal").modal("hide");
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//hàm load dữ liệu vào user table
function DisplayDataToTable(paramData) {
  "use strict";
  gSTT = 0; // thiết lập lại stt
  $("#chapter-table").DataTable().clear(); // xóa dữ liệu
  $("#chapter-table").DataTable().rows.add(paramData); //thêm các dòng dữ liệu vào datatable
  $("#chapter-table").DataTable().draw(); //vẽ lại bảng
}

// hàm get chapter by id
function getChapterById(paramId) {
  $.ajax({
    url: gBASE_URL + "/detail/" + paramId,
    type: "GET",
    success: function (paramRes) {
      gChapterObject = paramRes;
      showDataToModalUpdate(paramRes);
    },
    error: function (paramErr) {
      console.log(paramErr.status);
    },
  });
  // hiển thị modal lên
  $("#update-chapter-modal").modal("show");
}

// hàm show obj lên modal update
function showDataToModalUpdate(paramObj) {
  $("#update-chapterCode").val(paramObj.chapterCode);
  $("#update-chapterName").val(paramObj.chapterName);
  $("#update-chapterIntro").val(paramObj.chapterIntro);
  $("#update-nameTranslator").val(paramObj.nameTranslator);
  $("#update-chapterPage").val(paramObj.chapterPage);
}

// hàm thu thập dữ liệu để update
function getCreateData(paramObj) {
  paramObj.chapterCode = $("#create-chapterCode").val().trim();
  paramObj.chapterName = $("#create-chapterName").val().trim();
  paramObj.chapterIntro = $("#create-chapterIntro").val().trim();
  paramObj.nameTranslator = $("#create-nameTranslator").val().trim();
  paramObj.chapterPage = parseInt($("#create-chapterPage").val().trim());
}

// hàm validate data
function validateData(paramObj) {
  if (paramObj.chapterCode === "") {
    alert("Please fill chapter code!");
    return false;
  }
  for (var i = 0; i < gListChapterObjects.length; i++) {
    if (
      paramObj.chapterCode == gListChapterObjects[i].chapterCode &&
      gId != gListChapterObjects[i].id
    ) {
      alert("Chapter code already exists!");
      return false;
    }
  }
  if (paramObj.chapterName === "") {
    alert("Please fill chapter name!");
    return false;
  }
  if (paramObj.chapterIntro === "") {
    alert("Please fill chapter introduction!");
    return false;
  }
  if (paramObj.nameTranslator === "") {
    alert("Please fill name translator!");
    return false;
  }
  if (paramObj.chapterPage <= 0) {
    alert("Please fill chapter page!");
    return false;
  }
  return true;
}

//hàm confirm create thành công
function handleCreateSuccess() {
  alert("Confirmed create data chapter successfully!");
  callApiToGetListData();
  resetCreateForm();
  $("#create-chapter-modal").modal("hide");
}

//hàm confirm edit thành công
function handleUpdateSuccess() {
  alert("Confirmed update data chapter successfully!");
  callApiToGetListData();
  resetEditForm();
  $("#update-chapter-modal").modal("hide");
}

// hàm xóa trắng form edit modal
function resetCreateForm() {
  $("#create-chapterCode").val("");
  $("#create-chapterName").val("");
  $("#create-chapterIntro").val("");
  $("#create-nameTranslator").val("");
  $("#create-chapterPage").val("");
}

// hàm xóa trắng form edit modal
function resetEditForm() {
  $("#update-chapterCode").val("");
  $("#update-chapterName").val("");
  $("#update-chapterIntro").val("");
  $("#update-nameTranslator").val("");
  $("#update-chapterPage").val("");
  location.reload();
}

// hàm thu thập dữ liệu để update chapter
function getUpdateData(paramObj) {
  paramObj.chapterCode = $("#update-chapterCode").val().trim();
  paramObj.chapterName = $("#update-chapterName").val().trim();
  paramObj.chapterIntro = $("#update-chapterIntro").val().trim();
  paramObj.nameTranslator = $("#update-nameTranslator").val().trim();
  paramObj.chapterPage = parseInt($("#update-chapterPage").val().trim());
}

// hàm get chapter by id
function deleteChapterById(paramId) {
  $.ajax({
    url: gBASE_URL + "/delete/" + paramId,
    type: "DELETE",
    async: false,
    success: function (paramRes) {
      console.log(paramRes);
      handleDeleteSuccess();
    },
    error: function (paramErr) {
      console.log(paramErr.status);
    },
  });
}

//hàm confirm delete thành công
function handleDeleteSuccess() {
  alert("Delete data chapter successfully!");
  callApiToGetListData();
  $("#delete-chapter-modal").modal("hide");
}
