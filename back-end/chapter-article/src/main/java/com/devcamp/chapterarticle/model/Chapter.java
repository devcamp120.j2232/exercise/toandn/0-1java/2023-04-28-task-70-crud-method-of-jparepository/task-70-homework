package com.devcamp.chapterarticle.model;

import javax.persistence.*;
import java.util.*;
import javax.validation.constraints.*;

import org.hibernate.validator.constraints.Range;

@Entity
@Table(name = "chapter")

public class Chapter {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @NotEmpty(message = "You should fill chapter code!")
  @Size(min = 2, message = "Chapter code must be 2 characters!")
  @Column(name = "chapter_code", unique = true)
  private String chapterCode;

  @NotNull(message = "You should fill chapter name!")
  @Size(min = 2, message = "Chapter name must be 2 characters!")
  @Column(name = "chapter_name")
  private String chapterName;

  @NotNull(message = "You should fill chapter introduction!")
  @Size(min = 2, message = "Chapter introduction must be 2 characters!")
  @Column(name = "chapter_intro")
  private String chapterIntro;

  @NotNull(message = "You should fill name translator!")
  @Size(min = 2, message = "Name translator must be 2 characters!")
  @Column(name = "name_translator")
  private String nameTranslator;

  @NotNull(message = "You should fill chapter page!")
  @Range(min = 1, message = "Must fill value greater than 1")
  @Column(name = "chapter_page")
  private Long chapterPage;

  @OneToMany(targetEntity = Article.class, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  @JoinColumn(name = "chapter_id")
  private Set<Article> articles;

  public Chapter() {
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getChapterCode() {
    return chapterCode;
  }

  public void setChapterCode(String chapterCode) {
    this.chapterCode = chapterCode;
  }

  public String getChapterName() {
    return chapterName;
  }

  public void setChapterName(String chapterName) {
    this.chapterName = chapterName;
  }

  public String getChapterIntro() {
    return chapterIntro;
  }

  public void setChapterIntro(String chapterIntro) {
    this.chapterIntro = chapterIntro;
  }

  public String getNameTranslator() {
    return nameTranslator;
  }

  public void setNameTranslator(String nameTranslator) {
    this.nameTranslator = nameTranslator;
  }

  public Long getChapterPage() {
    return chapterPage;
  }

  public void setChapterPage(Long chapterPage) {
    this.chapterPage = chapterPage;
  }

  public Set<Article> getArticles() {
    return articles;
  }

  public void setArticles(Set<Article> articles) {
    this.articles = articles;
  }

}
