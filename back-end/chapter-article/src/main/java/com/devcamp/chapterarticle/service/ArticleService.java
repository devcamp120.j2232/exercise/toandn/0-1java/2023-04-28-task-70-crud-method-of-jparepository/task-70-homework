package com.devcamp.chapterarticle.service;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.chapterarticle.model.Article;
import com.devcamp.chapterarticle.repository.IArticleRepository;

@Service
public class ArticleService {
  @Autowired
  private IArticleRepository pIArticleRepository;

  public ArrayList<Article> getAllArticles() {
    ArrayList<Article> articleList = new ArrayList<>();
    pIArticleRepository.findAll().forEach(articleList::add);
    return articleList;
  }

}
