package com.devcamp.chapterarticle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChapterArticleApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChapterArticleApplication.class, args);
	}

}
