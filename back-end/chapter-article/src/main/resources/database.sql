
--
-- Đang đổ dữ liệu cho bảng `chapter`
--
INSERT INTO `chapter` (`id`, `chapter_code`, `chapter_name`, `chapter_intro`, `name_translator`, `chapter_page`) VALUES
(01, 'C2D', 'Cách tạo thiện cảm', 'Hãy là chính mình', 'Nguyễn Văn A', '70'),
(02, 'C7R', 'Làm việc để học - Đừng làm việc vì tiền', 'Giá trị của làm việc', 'Phạm Thị B', '50'),
(03, 'C3U', 'Hiểu được cảm xúc', 'Mở rộng hiểu biết', 'Lê Chí C', '40'),
(04, 'C1F', 'Kẻ thù là bạn', 'Bạn cần điều gì', 'Thái tự D', '5'),
(05, 'C4K', 'Xem lại bạn thân', 'Mở rộng tầm nhìn', 'Vũ Văn E', '35'),
(06, 'C5J', 'Tình yêu là gì', 'Tâm bạn đã sẵn sàng', 'Lưu Sơn F', '25');

--
-- Đang đổ dữ liệu cho bảng `article`
--
INSERT INTO `article` (`id`, `article_code`, `article_name`, `article_intro`, `article_page`, `chapter_id`) VALUES
(01, 'A6D', 'Làm sao cho người ta ưa mình liền', 'Cảm nhận về bản thân', '100', 1),
(02, 'A1R', 'Nhìn nhận năng lực', 'Tư duy đa hướng', '80', 2),
(03, 'A4U', 'Ai nhận ai cho', 'Cảm nhận con tim và lý trí', '60', 3),
(04, 'A3F', 'Nên đối mặt nỗi khiếp sợ', 'Ngòi nổ bứt phá', '20', 4),
(05, 'A2K', 'Kiểm chứng nỗi sợ', 'Khoanh vùng điểm chết', '40', 5),
(06, 'A7J', 'Thật lòng với con tim', 'Nới hai tâm hồn đồng điệu', '50', 6);
